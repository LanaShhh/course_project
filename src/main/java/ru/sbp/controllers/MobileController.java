package ru.sbp.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.headers.Header;
import jakarta.ws.rs.HeaderParam;
import lombok.RequiredArgsConstructor;
//import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import ru.sbp.apihandlers.AuthHandler;
import ru.sbp.objects.structures.OrderStructure;
import ru.sbp.apihandlers.RequestHandler;
import ru.sbp.objects.stats.QrStatistics;
import ru.sbp.objects.structures.SecretKey;
import ru.sbp.objects.auth.JwtRequest;
import ru.sbp.objects.auth.JwtResponse;
import ru.sbp.objects.db.Order;
import ru.sbp.objects.db.QR;
import ru.sbp.objects.db.User;
import ru.sbp.security.JwtEntity;
import ru.sbp.storage.Context;
import ru.sbp.utils.DbHandler;

import java.nio.file.AccessDeniedException;
import java.util.List;

import static ru.sbp.utils.DbHandler.*;

@RestController
@RequiredArgsConstructor
@Tag(name="Mobile", description="Ручки, которые используются приложением для взаимодействия")
public class MobileController {
    @Autowired
    private Context context;

    private final AuthHandler authHandler;
    private final PasswordEncoder passwordEncoder;

//    private static final org.apache.logging.log4j.core.Logger logger = (Logger) LogManager.getLogger(MobileController.class);
    @Operation(
            summary = "Создание заказа",
            description = "Создает заказ на указанном QR-коде"
    )
    @PostMapping(path = "/createOrder")
    public ResponseEntity<OrderStructure> createOrder(@RequestBody  @Parameter(description = "Данные заказа", required = true) OrderStructure newOrder,
                                                       Authentication authentication) {
        QR qr = getQrByOrder(newOrder);
        Long userId = ((JwtEntity) authentication.getPrincipal()).getId();
        User user = DbHandler.getUserById(userId);
        if (qr.getCurrentOrderId() == null || qr.getCurrentOrderId().isEmpty()) {
            var res = RequestHandler.createNewOrder(newOrder, user.getSecretKey());
            changeCurrentOrderId(res.getBody());
            return ResponseEntity.ok().body(res.getBody());
        } else {
            try {
                RequestHandler.deleteOrder(qr.getCurrentOrderId(), user.getSecretKey());
            } catch (Exception ignored) {
            }
            var res = RequestHandler.createNewOrder(newOrder, user.getSecretKey());
            changeCurrentOrderId(res.getBody());
            res.getBody().getQr().setRedirectUrlId(qr.getRedirectUrlId());
            res.getBody().getQr().setFiscType(qr.getFiscType());
            return ResponseEntity.ok().body(res.getBody());
        }
    }

    @Operation(
            summary = "Смена типа фискализации",
            description = "Меняет тип фискализации на указанный. " +
                          "NOFISC - нет фискализации, FISC105 - ФФД 1.05, FISC12 - ФФД 1.2. " +
                          "Изначально фискализация на QR-коде отключена"
    )
    @PutMapping(path = "/{qrId}/setFiscType/{fiscType}")
    public ResponseEntity<String> setFiscType(@PathVariable @Parameter(description = "ID QR-кода", example = "YU6ygh") String qrId,
                                              @PathVariable @Parameter(description = "Тип фискализации", example = "NOFISC") String fiscType) {
        // no fisc - None/NOFISC, 1.05 - FISC105, 1.2 - FISC12
//        logger.info("setFiscType, login " + login + " fiscType " + fiscType);
        DbHandler.updateUserFiscType(fiscType, qrId);
        return ResponseEntity.ok().build();
    }

    @Operation(
            summary = "Информация о заказе",
            description = "Возвращает информацию об указанном заказе"
    )
    @GetMapping(path = "/getOrder/{qrId}")
    public ResponseEntity<OrderStructure> getOrder(@PathVariable @Parameter(description = "ID QR-кода", example = "JI582") String qrId,
                                                   Authentication authentication) {
//        logger.info("Get order request, qrId " + qrId);
        QR qr = getQrById(qrId);
        Long userId = ((JwtEntity) authentication.getPrincipal()).getId();
        User user = DbHandler.getUserById(userId);
        DbHandler.linkQrWithUser(qrId, userId);
        if (qr.getCurrentOrderId() == null || qr.getCurrentOrderId().isEmpty()) {
            return ResponseEntity.ok().body(new OrderStructure(qr));
        } else {
            var result = RequestHandler.getOrder(qr.getCurrentOrderId(), user.getSecretKey());
            result.getBody().getQr().setRedirectUrlId(qr.getRedirectUrlId());
            result.getBody().getQr().setFiscType(qr.getFiscType());
            return result;
        }
    }

    @Operation(
            summary = "Сохранить secret key",
            description = "Привязывает ключ из аккаунта Raif Pay к пользователю."
    )
    @PutMapping(path = "/setSecretKey")
    public ResponseEntity<String> changeSecretKey(@RequestBody SecretKey body, Authentication authentication) {
        Long userId = ((JwtEntity) authentication.getPrincipal()).getId();
        try {
            RequestHandler.linkUrl(body.getSecretKey(), context.getCallbackServerAddress());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        DbHandler.setSecretKey(userId, body.getSecretKey());
        return ResponseEntity.ok().build();
    }

    @Operation(
            summary = "Получить статистику",
            description = "Получение данных по QR-коду, а именно общей суммы на заказах, числа заказов и список заказов. " +
                          "При вызове метода очистки статистики данные обнуляются."
    )
    @GetMapping(path = "/getStats/{qrId}")
    public ResponseEntity<QrStatistics> getStats(@PathVariable @Parameter(description = "ID QR-кода", example = "HJ1339") String qrId){
        QR qr = getQrById(qrId);
        List<Order> orderList = getOrdersByQrId(qrId);
        return ResponseEntity.ok().body(new QrStatistics(qr.getTotalSum(), qr.getOrderCount(), orderList));
    }

    @Operation(
            summary = "Очистить статистику",
            description = "Обнуление сохраненной статистики по QR-коду"
    )
    @DeleteMapping (path = "/clearStats/{qrId}")
    public ResponseEntity<String> clearStats(@PathVariable @Parameter(description = "ID QR-кода", example = "VG78") String qrId){
        DbHandler.clearStats(qrId);
        return ResponseEntity.ok().build();
    }

    @Operation(
            summary = "Log in",
            description = "Вход пользователя по паролю и логину. Метод возвращает access token и refresh token для доступа к остальным методам."
    )
    @PostMapping (path = "/auth/login")
    public ResponseEntity<JwtResponse> login(@RequestBody JwtRequest loginRequest){
        authHandler.login(loginRequest);
        return ResponseEntity.ok().body(authHandler.login(loginRequest));
    }

    @Operation(
            summary = "Регистрация",
            description = "Регистрация пользователя. Ожидается получение логина и пароля."
    )
    @PostMapping (path = "/auth/register")
    public ResponseEntity<User> register(@RequestBody User user){
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        DbHandler.createUser(user);
        return ResponseEntity.ok().build();
    }

    @Operation(
            summary = "Удаление QR-кода из базы",
            description = "После вызова прекращается сбор информации о данном QR-коде пользователя. " +
                          "QR-код можно будет привязать повторно, но данные по нему будут удалены."
    )
    @DeleteMapping (path = "/removeLink/{qrId}")
    public ResponseEntity<String> removeLink(@PathVariable @Parameter(description = "ID QR-кода", example = "LK898") String qrId,
                                             Authentication authentication){
        Long userId = ((JwtEntity) authentication.getPrincipal()).getId();
        DbHandler.deleteLink(userId, qrId);
        return ResponseEntity.ok().build();
    }

    @Operation(
            summary = "Обновление ",
            description = "После вызова прекращается сбор информации о данном QR-коде пользователя. " +
                    "QR-код можно будет привязать повторно, но данные по нему будут удалены."
    )
    @PostMapping("/auth/refresh")
    public ResponseEntity<JwtResponse> refresh(@RequestBody String refreshToken) throws AccessDeniedException {
        return ResponseEntity.ok().body(authHandler.refresh(refreshToken));
    }
}
