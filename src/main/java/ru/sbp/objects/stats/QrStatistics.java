package ru.sbp.objects.stats;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.sbp.objects.db.Order;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Данные о статистике QR-кода")
public class QrStatistics {
    @Schema(description = "Общая сумма заказов", example = "305.0")
    private Double totalSum;
    @Schema(description = "Количество заказов", example = "1")
    private Integer orderCount;
    @Schema(description = "Список заказов")
    private List<Order> orders;
}
