package ru.sbp.objects.structures;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class StatusStructure {
    @Schema(example="DONE")
    private String value;

}
