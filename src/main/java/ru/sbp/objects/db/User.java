package ru.sbp.objects.db;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
@Schema(description = "Данные о пользователе в базе")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Schema(description = "Уникальный id в базе", example = "1")
    private Long id;
    @Schema(description = "Логин", example = "ivanov1995")
    private String username;
    @Schema(description = "Пароль", example = "12345")
    private String password;
    @Schema(description = "Ключ из аккаунта Raif Pay", example = "12345")
    private String secretKey;

    public User(String username){
        this.username = username;
    }

    public User(String username, String secretKey){
        this.username = username;
        this.secretKey = secretKey;
    }
}
