package ru.sbp.objects.db;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "orders")
@Schema(description = "Данные о заказе")
public class Order {
    @Id
    @Schema(description = "ID заказа", example = "ABC123")
    private String orderId;
    @Schema(description = "ID QR-кода", example = "BCA321")
    private String qrId;
    @Schema(description = "Тип фискализации, None/NOFISC, FISC105, FISC12", example = "NOFISC")
    private String fiscalization;
    @Schema(description = "ID чека по заказу", example = "QWERTY123")
    private String receiptId;
    @Schema(description = "Номер телефона покупателя", example = "+79999999999")
    private String phone;
    @Schema(description = "Отправлен ли чек покупателю")
    private boolean isDone;
    @Schema(description = "Время оплаты покупки")
    private Timestamp buyDate;
    @Schema(description = "Сумма заказа")
    private double amount;


    public boolean getIsDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public void updateOrder(Order order) {
        this.qrId = order.getQrId();
        this.fiscalization = order.getFiscalization();
        this.receiptId = order.getReceiptId();
        this.phone = order.getPhone();
        this.isDone = order.getIsDone();
        this.buyDate = order.getBuyDate();
        this.amount = order.getAmount();
    }
}
