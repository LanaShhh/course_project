package ru.sbp.objects.auth;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description = "Данные, необходимые для получения access и refresh токенов.")
public class JwtRequest {
    @Schema(description = "Логин", example = "ivanov1995")
    private String username;
    @Schema(description = "Пароль", example = "12345")
    private String password;
}
