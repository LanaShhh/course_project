package ru.sbp.objects.auth;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import ru.sbp.objects.db.QR;

import java.util.List;

@Data
@Schema(description = "Ответ от микросервиса при запросе access и refresh токенов, а также при логине.")
public class JwtResponse {
    @Schema(description = "id в базе", example = "1")
    private Long id;
    @Schema(description = "Логин", example = "ivanov1995")
    private String username;
    @Schema(description = "access token", example = "12345")
    private String accessToken;
    @Schema(description = "refresh token", example = "54321")
    private String refreshToken;
    private List<QR> qrs;
}
