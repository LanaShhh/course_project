package ru.sbp.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;

@OpenAPIDefinition(
        info = @Info(
                title = "SBP microservice API",
                description = "Open source contactless payment service (NFC) via Faster payment system (FPS) with automatic fiscalization, microservice API",
                version = "1.0.0",
                contact = @Contact(
                        name = "Zhumlyakova Svetlana",
                        email = "qrcodereceiptsender@gmail.com",
                        url = "https://gitlab.com/LanaShhh"
                )
        )
)
public class OpenApiConfig {

}